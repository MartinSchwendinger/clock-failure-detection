library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.clk_process_pkg.all;

-- This testbench tests the leaf clock monitoring circuit with flip flops and Muller-C elements. See figure 8 in the "elaborated_circuits_overview.pdf" file.
entity leaf_clock_monitoring_FF_Muller_C_circuit_tb is
end entity;

architecture bench of leaf_clock_monitoring_FF_Muller_C_circuit_tb is

	component leaf_clock_monitoring_FF_Muller_C_circuit
		generic(
			N : integer										-- Number of leaf clocks to monitor.
		);
		port(
			res : in std_logic;
			clk : in std_logic_vector(N downto 0);
		
			monitor : out  std_logic
		);
	end component;
	
	constant N : integer := 3;							-- We go with 3 leaf clocks here.
	type TIME_ARRAY is array (N downto 0) of time;
	constant CLK_PERIOD : TIME_ARRAY := (0 => 11 ns, 1 => 13 ns, 2 => 17 ns, others => 20 ns); 	-- Configure different clock periods.
	
	signal clk : std_logic_vector(N downto 0);
	signal res : std_logic;
	signal monitor : std_logic;
	signal stop : std_logic_vector(N downto 0);
	
begin
	
	uut : leaf_clock_monitoring_FF_Muller_C_circuit
		generic map(
			N => N
		)
		port map (
			res => res,
			clk => clk,
			monitor => monitor
		);
		
	clks : for i in N downto 0 generate 					-- Generate N leaf clocks, with the option to stop them at purpose.
		clk_proc : clk_process
			generic map(
				CLK_PERIOD => CLK_PERIOD(i)
			)
			port map(
				stop => stop(i),
				clk => clk(i)
			);
	end generate;
	
	
	inst : process
	begin
		res <= '1';
		stop <= (others => '0');
		wait for 50 ns;										-- Wait 50 ns until the reset is released.	
		res <= '0';
		wait for 200 ns;									-- Wait additional 200 ns until a clock fautlt is injected. You may change this value to see clock faults in different states.
		stop(2) <= '1';										-- Stop the second leaf clock.
		wait;
	end process;

end architecture;