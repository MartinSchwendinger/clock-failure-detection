library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- This testbench tests the pure delay circuit. See figure 1 in the "elaborated_circuits_overview.pdf" file.
entity pure_delay_circuit_tb is
end entity;

architecture bench of pure_delay_circuit_tb is

	component pure_delay_circuit
		generic(
			DELAY : time
		);
		port(
		clk_DUT : in std_logic;
		res : in std_logic;
      
		err : out std_logic
		);
	end component;
	
	signal clk_DUT : std_logic;
	signal res : std_logic;
	signal err : std_logic;
	signal stop : std_logic;
	
	constant CLK_DUT_PERIOD : time := 40 ns;
	constant DELAY : time := CLK_DUT_PERIOD * 0.75;		-- The optimal delay of the delay element.
	
begin
	
	uut : pure_delay_circuit
		generic map(
			DELAY => DELAY
		)
		port map (
			clk_DUT => clk_DUT,
			res => res,
			err => err
		);
		
	clk_int : process									-- The target clock. The stop signal forces the clock to stick at its current value.
	begin
		clk_DUT <= '0';
		if stop = '1' then
			wait;
		end if;
		wait for CLK_DUT_PERIOD/2;
		clk_DUT <= '1';
		if stop = '1' then
			wait;
		end if;
		wait for CLK_DUT_PERIOD/2;
	end process;
	
	inst : process
	begin
		res <= '1';
		stop <= '0';
		wait for 45 ns;								  -- Wait 45 ns until the reset is released.
		res <= '0';
		wait for 80 ns;								  -- Wait additional 80 ns until a clock fautlt is injected. You may change this value to see clock faults in different states.
		stop <= '1';
		wait;
	end process;

end architecture;