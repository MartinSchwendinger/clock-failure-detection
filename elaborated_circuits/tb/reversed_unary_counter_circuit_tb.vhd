library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reversed_unary_counter_circuit_tb is
end entity;

-- This testbench tests the reversed unary counter circuit. See figure 4 in the "elaborated_circuits_overview.pdf" file.
architecture bench of reversed_unary_counter_circuit_tb is

	component reversed_unary_counter_circuit
		port(
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err : out std_logic
		);
	end component;
	
	signal clk_DUT : std_logic;
	signal clk_ref : std_logic;
	signal res : std_logic;
	signal err : std_logic;
	signal stop : std_logic;
	
	constant CLK_DUT_PERIOD : time := 10 ns; 
	constant CLK_REF_PERIOD : time := 25 ns; 
	
begin
	
	uut : reversed_unary_counter_circuit
		port map (
			clk_DUT => clk_DUT,
			clk_ref => clk_ref,
			res => res,
			err => err
		);
		
	clk_ref_int : process					-- The reference clock.
	begin
		clk_ref <= '0';
		wait for CLK_REF_PERIOD/2;
		clk_ref <= '1';
		wait for CLK_REF_PERIOD/2;
	end process;
	
	clk_DUT_int : process					-- The target clock. The stop signal forces the clock to stick at its current value.
	begin
		clk_DUT <= '0';
		if stop = '1' then
			wait;
		end if;
		wait for CLK_DUT_PERIOD/2;
		clk_DUT <= '1';
		if stop = '1' then
			wait;
		end if;
		wait for CLK_DUT_PERIOD/2;
	end process;
	
	inst : process
	begin
		res <= '1';
		stop <= '0';
		wait for 10 ns;						-- Wait 10 ns until the reset is released.
		res <= '0';
		wait for 60 ns;						-- Wait additional 60 ns until a clock fautlt is injected. You may change this value to see clock faults in different states.
		stop <= '1';
		wait;
	end process;

end architecture;