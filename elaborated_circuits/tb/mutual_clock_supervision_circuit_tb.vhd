library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mutual_clock_supervision_circuit_tb is
end entity;

-- This testbench tests the mutual clock supervision circuit. See figure 6 in the "elaborated_circuits_overview.pdf" file.
architecture bench of mutual_clock_supervision_circuit_tb is

	component mutual_clock_supervision_circuit
		port(
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err_DUT : out std_logic;
		err_ref : out std_logic
		);
	end component;
	
	signal clk_DUT : std_logic;
	signal clk_ref : std_logic;
	signal res : std_logic;
	signal err_DUT : std_logic;
	signal err_ref : std_logic;
	signal stop_DUT : std_logic;
	signal stop_ref : std_logic;
	
	constant CLK_DUT_PERIOD : time := 40 ns; 
	constant CLK_REF_PERIOD : time := 28 ns; 
	
begin
	
	uut : mutual_clock_supervision_circuit
		port map (
			clk_DUT => clk_DUT,
			clk_ref => clk_ref,
			res => res,
			err_DUT => err_DUT,
			err_ref => err_ref
		);
		
	clk_ref_int : process					-- The reference clock. The stop signal forces the clock to stick at its current value.
	begin
		clk_ref <= '0';
		if stop_ref = '1' then
			wait;
		end if;
		wait for CLK_REF_PERIOD/2;
		clk_ref <= '1';
		if stop_ref = '1' then
			wait;
		end if;
		wait for CLK_REF_PERIOD/2;
	end process;
	
	clk_DUT_int : process					-- The target clock. The stop signal forces the clock to stick at its current value.
	begin
		clk_DUT <= '0';
		if stop_DUT = '1' then
			wait;
		end if;
		wait for CLK_DUT_PERIOD/2;
		clk_DUT <= '1';
		if stop_DUT = '1' then
			wait;
		end if;
		wait for CLK_DUT_PERIOD/2;
	end process;
	
	inst : process
	begin
		res <= '1';
		stop_DUT <= '0';
		stop_ref <= '0';
		wait for 20 ns;						-- Wait 20 ns until the reset is released.
		res <= '0';
		wait for 60 ns;						-- Wait additional 60 ns until a clock fautlt is injected. You may change this value to see clock faults in different states.
		stop_DUT <= '1';					-- You may also change this to stop_ref.
		wait;
	end process;

end architecture;