library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_process is
	generic(
		CLK_PERIOD : time
	);
	port (
		stop : in std_logic;
		clk : out std_logic
	);
end entity;
 
architecture beh of clk_process is
begin

	clk_int : process
	begin
		if stop = '0' then
			clk <= '0';
		end if;
		wait for CLK_PERIOD/2;
		
		if stop = '0' then
			clk <= '1';
		end if;
		wait for CLK_PERIOD/2;
	end process;
	
end architecture;