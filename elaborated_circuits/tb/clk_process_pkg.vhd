library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package clk_process_pkg is

	component clk_process is
		generic(
			CLK_PERIOD : time
		);
		port (
			stop : in std_logic;
			clk : out std_logic
		);
	end component;
end package;