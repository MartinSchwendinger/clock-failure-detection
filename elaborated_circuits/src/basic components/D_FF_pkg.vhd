library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package D_FF_pkg is

	component D_FF is
		port
		(
			clk : in std_logic;
			res : in std_logic;
      
			D : in std_logic;
			Q : out std_logic
		);
	end component;
end package;