library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- A very simple behavioural implementation of a Muller-C element.
entity Muller_C_element is
	
	port (
		A : in std_logic;
		B : in std_logic;
		res : in std_logic;
		
		R : out std_logic
	);
end entity Muller_C_element;
 
architecture beh of Muller_C_element is
begin

	R <= '0' when res = '1' else
		  A when A = B else
		  R;
	
end architecture beh;