library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package delay_sim_pkg is

	component delay_sim is
		generic(
			DELAY : time
		);
		port (
			A : in std_logic;
			R : out std_logic
		);
	end component;
end package;