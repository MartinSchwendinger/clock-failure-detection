library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package Muller_C_element_pkg is

	component Muller_C_element is
		port (
		A : in std_logic;
		B : in std_logic;
		res : in std_logic;
		
		R : out std_logic
		);
	end component;
end package;