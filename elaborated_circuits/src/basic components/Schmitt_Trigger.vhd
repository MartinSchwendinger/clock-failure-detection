library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- A dummy smitt trigger for a nicer RTL-View
entity Schmitt_Trigger is
	
	port (
		A : in std_logic;
		R : out std_logic := '0'
	);
end entity Schmitt_Trigger;
 
architecture beh of Schmitt_Trigger is
begin

	R <= '1' when A = '0' else
		 '0' when A = '1' else
		  R;
	
end architecture beh;