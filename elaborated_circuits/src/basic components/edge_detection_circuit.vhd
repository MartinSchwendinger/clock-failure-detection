library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.delay_sim_pkg.all;

-- This file represents a rising edge detector. Whenever rising_edge(clk) pulse becomes high for DELAY time. 
entity edge_detection_circuit is

	generic(
		DELAY : time := 5 ns
	);
	
	port (
		clk : in std_logic;
		pulse : out std_logic
	);
end entity edge_detection_circuit;
 
architecture beh of edge_detection_circuit is
	
	signal clk_delay : std_logic;

begin

	del : delay_sim
		generic map(
			DELAY => DELAY
		)
		port map(
			A => clk,
			R => clk_delay
		);
	
	pulse <= clk and not clk_delay;
		
end architecture;