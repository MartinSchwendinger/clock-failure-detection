library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package edge_detection_circuit_pkg is

	component edge_detection_circuit is
		generic(
			DELAY : time
		);
	
		port (
			clk : in std_logic;
			pulse : out std_logic
		);
	end component;
end package;