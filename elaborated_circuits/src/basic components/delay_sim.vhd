library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- This file implements a signal delay only for simulation.
-- A synthesizable delay is implemented in file "delay_quartus.vhd".
entity delay_sim is

	generic(
		DELAY : time := 60 ns
	);
	
	port (
		A : in std_logic;
		R : out std_logic
	);
end entity;
 
architecture arch of delay_sim is
	
begin

	R <= transport A after DELAY;
		
end architecture;