library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package Schmitt_Trigger_pkg is

	component Schmitt_Trigger is
		port (
		A : in std_logic;
		R : out std_logic
		);
	end component;
end package;