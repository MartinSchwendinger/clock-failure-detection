library ieee;
use ieee.std_logic_1164.all;

-- A basic synchronizer
entity sync is
	generic (
		SYNC_STAGES : integer range 2 to integer'high;
		RESET_VALUE : std_logic
	);
	port (
		clk   : in std_logic;
		res : in std_logic;

		data_in   : in std_logic;
		data_out  : out std_logic
	);
end entity;

architecture beh of sync is
	signal sync : std_logic_vector(1 to SYNC_STAGES);
begin
	sync_proc : process(clk, res)
	begin
		if res = '1' then
			sync <= (others => RESET_VALUE);
		elsif rising_edge(clk) then
			sync(1) <= data_in; 
			for i in 2 to SYNC_STAGES loop
				sync(i) <= sync(i - 1);
			end loop;
		end if;
	end process sync_proc;
	
	data_out <= sync(SYNC_STAGES);
end architecture;
