library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- This entity represents a very simple D-Flip-Flop. Of course, also the primitive D-Flip-Flop could be taken and this entity will in fact be synthesized to one.
-- However, when using the RTL-View this entity represents a D-Flip-Flop quite nice there.
entity D_FF is
	port
	(
		clk : in std_logic;
		res : in std_logic;
      
		D : in std_logic;
		Q : out std_logic
	);
end entity D_FF;
 
architecture beh of D_FF is

begin
	process (all) is
    begin
		if res = '1' then
			Q <= '0';
		elsif rising_edge(clk) then  
			Q <= D;
        end if;
	end process;
end architecture beh;