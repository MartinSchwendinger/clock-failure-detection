library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;
use work.Muller_C_element_pkg.all;

-- A Muller-C element and a flip flog arranged like following:
--    ____________________________
--   |                            |
--   |     _______                |
--   |    |       |               |
--   |---O|D     Q|---|           |
--        |       |   |           |
-- clk----|>      |   |           |
--        |_______|   |           |
--                    |           |
--                    |           |
--      ______________|           |
--     |                          |
--     |   _______                |
--     |  |       \               |
--     |--|        \              |
--        |    C    |---------------- R
--    B --|        /
--        |_______/

entity FF_Muller_C is
	port (
		clk : in std_logic;
		res : in std_logic;
		B 	: in std_logic;
		R 	: out std_logic
	);
end entity;
 
architecture beh of FF_Muller_C is

	signal R_inv : std_logic;
	signal Q : std_logic;

begin

	R_inv <= not R;

	Muller_C_1 : Muller_C_element
		port map(
			A => Q,
			B => B,
			res => res,
			R => R
		);
	
	flip_flop_1 : D_FF 
		port map (
			clk => clk,
			res => res,
      
			D => R_inv,
			Q => Q
		);
		
end architecture;