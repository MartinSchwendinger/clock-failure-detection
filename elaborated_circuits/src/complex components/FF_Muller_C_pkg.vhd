library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package FF_Muller_C_pkg is

	component FF_Muller_C is
		port (
			clk : in std_logic;
			res : in std_logic;
			B 	: in std_logic;
			R 	: out std_logic
		);
	end component;
end package;