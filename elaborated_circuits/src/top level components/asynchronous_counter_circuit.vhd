library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;
use work.Muller_C_element_pkg.all;

-- This entity represents the asynchronous counter circuit. See figure 6 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "asynchronous_counter_circuit_tb.vhd" file.
entity asynchronous_counter_circuit is
	port (
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err : out std_logic
	);
end entity asynchronous_counter_circuit;
 
architecture beh of asynchronous_counter_circuit is

	signal MC1 : std_logic;
	signal MC2 : std_logic;
	
	signal MC2_inv : std_logic;
	signal clk_ref_inv : std_logic;
	
	signal Q1 : std_logic;
	signal Q2 : std_logic;
	

begin

	MC2_inv <= not MC2;
	
	clk_ref_inv <= not clk_ref;

	err <= (not Q1) or (Q2); 

	Muller_C_1 : Muller_C_element
		port map(
			A => clk_DUT,
			res => res,
			B => MC2_inv,
			R => MC1
		);
		
	Muller_C_2 : Muller_C_element
		port map(
			A => MC1,
			res => res,
			B => clk_ref_inv,
			R => MC2
		);
		
	--Muller_C_3 : Muller_C_element			--Maybe a longer chain is needed
	--	port map(
	--		A => MC2,
	--		B => MC4_inv,
	--		res => res,
	--		R => MC3
	--	);
		
	--Muller_C_4 : Muller_C_element
	--	port map(
	--		A => MC3,
	--		B => clk_ref_inv,
	--		res => res,
	--		R => MC4
	--	);	
	
	flip_flop_1 : D_FF 
		port map (
			clk => clk_ref,
			res => res,
      
			D => MC2,
			Q => Q1
		);
		
	flip_flop_2 : D_FF 
		port map (
			clk => clk_ref_inv,
			res => res,
      
			D => MC2,
			Q => Q2
		);
		
		
end architecture beh;