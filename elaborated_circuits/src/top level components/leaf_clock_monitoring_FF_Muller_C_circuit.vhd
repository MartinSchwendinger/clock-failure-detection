library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.FF_Muller_C_pkg.all;

-- This entity represents the leaf clock monitoring circuit with flip flops and Muller-C elements. See figure 8 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "leaf_clock_monitoring_FF_Muller_C_circuit_tb.vhd" file.
entity leaf_clock_monitoring_FF_Muller_C_circuit is
	generic(
		N : integer := 4
	);
	port (
		res : in std_logic;
		clk : in std_logic_vector(N downto 0);
		
		monitor : out std_logic
	);
end entity leaf_clock_monitoring_FF_Muller_C_circuit;
 
architecture beh of leaf_clock_monitoring_FF_Muller_C_circuit is

	signal Q : std_logic_vector(N + 1 downto 0);

begin

	monitor <= Q(0);
	
	Q(0) <= not Q(N + 1);
	
	FF_Ms : for i in N downto 0 generate
		FF_M : FF_Muller_C 
			port map (
				clk => clk(i),
				res => res,
      
				B => Q(i),
				R => Q(i + 1)
		);
	end generate;
		
end architecture beh;