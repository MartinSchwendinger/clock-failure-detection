library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;
use work.edge_detection_circuit_pkg.all;

-- This entity represents the mutual clock supervision circuit. See figure 5 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "mutual_clock_supervision_circuit_tb.vhd" file.
entity mutual_clock_supervision_circuit is
	generic(
		PULSE_DURATION : time := 5 ns		-- The duration of the pulse beeing high.
	);
	port (
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err_DUT : out std_logic;
		err_ref : out std_logic
	);
end entity mutual_clock_supervision_circuit;
 
architecture beh of mutual_clock_supervision_circuit is

	signal Q1_DUT : std_logic;
	signal Q1_ref : std_logic;
	signal clk_DUT_rising_edge : std_logic;
	signal clk_ref_rising_edge : std_logic;	
	
	signal QM_clk_ref_2 : std_logic;
	signal QM_clk_ref_2_inv : std_logic;

begin

	QM_clk_ref_2_inv <= not QM_clk_ref_2;
	
	flip_flop_DUT1 : D_FF 
		port map (
			clk => clk_DUT,
			res => clk_ref_rising_edge,
      
			D => '1',
			Q => Q1_DUT
		);
		
	flip_flop_DUT2 : D_FF 
		port map (
			clk => QM_clk_ref_2,
			res => res,
      
			D => Q1_DUT,
			Q => err_DUT
		);
		
	clk_ref_edge_detector : edge_detection_circuit
		generic map(
			DELAY => PULSE_DURATION
		)
		port map(
			clk => QM_clk_ref_2,
			pulse => clk_ref_rising_edge
		);
		
	flip_flop_MID : D_FF 
		port map (
			clk => clk_ref,
			res => res,
      
			D => QM_clk_ref_2_inv,
			Q => QM_clk_ref_2
		);
		
	flip_flop_ref1 : D_FF 
		port map (
			clk => clk_ref,
			res => clk_DUT_rising_edge,
      
			D => '1',
			Q => Q1_ref
		);
		
	flip_flop_ref2 : D_FF 
		port map (
			clk => clk_DUT,
			res => res,
      
			D => Q1_ref,
			Q => err_ref
		);
		
	clk_DUT_edge_detector : edge_detection_circuit
		generic map(
			DELAY => PULSE_DURATION
		)
		port map(
			clk => clk_DUT,
			pulse => clk_DUT_rising_edge
		);
		
end architecture beh;