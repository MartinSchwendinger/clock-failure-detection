library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.edge_detection_circuit_pkg.all;

-- This entity represents the binary counter circuit. See figure 2 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "binary_counter_circuit_tb.vhd" file.
entity binary_counter_circuit is

	generic(
		TH : integer;					-- The threshold value
		PULSE_DURATION : time := 5 ns	-- The duration of the pulse beeing high
	);
	port
	(
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err : out std_logic
	);
end entity binary_counter_circuit;
 
architecture beh of binary_counter_circuit is

	signal cnt : integer range 0 to TH := 0;
	signal res_sig : std_logic;
	signal pulse : std_logic;

begin

	res_sig <= res or pulse;

	edge_detection : edge_detection_circuit
		generic map(
			DELAY => PULSE_DURATION
		)
		port map(
			clk => clk_DUT,
			pulse => pulse
		);
	
	binary_counter_implementation : process(all) is
	begin
		if res_sig = '1' then
			cnt <= 0;
			err <= '0';
		elsif cnt >= TH then
			err <= '1';
		elsif rising_edge(clk_ref) then
			cnt <= cnt + 1;
		end if;
	end process;
	
end architecture beh;