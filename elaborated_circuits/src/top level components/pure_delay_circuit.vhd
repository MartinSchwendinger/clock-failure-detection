library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;
use work.delay_sim_pkg.all;

-- This entity represents the pure delay circuit. See figure 1 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "pure_delay_circuit_tb.vhd" file.
entity pure_delay_circuit is
	generic(
		DELAY : time		-- The delay of the delay element Δ.
	);
	port
	(
		clk_DUT : in std_logic;
		res : in std_logic;
      
		err : out std_logic
	);
end entity pure_delay_circuit;
 
architecture beh of pure_delay_circuit is

	signal clk_D : std_logic;
	signal clk_D_inv : std_logic;
	signal clk_DUT_inv : std_logic;
	signal err_1_sig : std_logic;
	signal err_2_sig : std_logic;

begin

	clk_DUT_inv <= not clk_DUT;
	clk_D_inv <= not clk_D;
	err <= err_1_sig or err_2_sig;
	
	del : delay_sim
		generic map (
			DELAY => DELAY
		)
		port map(
			A => clk_DUT,
			R => clk_D
		);
	
	flip_flop_1 : D_FF 
		port map (
			clk => clk_D,
			res => res,
      
			D => clk_DUT,
			Q => err_1_sig
		);
		
	flip_flop_2 : D_FF 
		port map (
			clk => clk_D_inv,
			res => res,
      
			D => clk_DUT_inv,
			Q => err_2_sig
		);
		
end architecture beh;