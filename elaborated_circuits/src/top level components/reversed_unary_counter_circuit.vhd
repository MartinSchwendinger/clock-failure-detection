library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;
use work.edge_detection_circuit_pkg.all;

-- This entity represents the reversed unary counter circuit. See figure 4 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "reversed_unary_counter_circuit_tb.vhd" file.
entity reversed_unary_counter_circuit is
	generic(
		PULSE_DURATION : time := 5 ns   -- The duration of the pulse beeing high.
	);
	port (
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err : out std_logic
	);
end entity;
 
architecture beh of reversed_unary_counter_circuit is

	signal res_ref : std_logic;
	signal pulse : std_logic;
	signal Q : std_logic;

begin

	res_ref <= res or pulse;
	
	edge_detection : edge_detection_circuit
		generic map(
			DELAY => PULSE_DURATION
		)
		port map(
			clk => clk_ref,
			pulse => pulse
		);
	
	flip_flop_1 : D_FF 
		port map (
			clk => clk_DUT,
			res => res_ref,
      
			D => '1',
			Q => Q
		);
		
	flip_flop_2 : D_FF 
		port map (
			clk => clk_ref,
			res => res,
      
			D => Q,
			Q => err
		);
		
end architecture beh;