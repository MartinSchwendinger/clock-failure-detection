library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;

-- This entity represents the unary counter circuit. See figure 3 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "unary_counter_circuit_tb.vhd" file.
entity unary_counter_circuit is
	port (
		clk_DUT : in std_logic;
		clk_ref : in std_logic;
		res : in std_logic;
      
		err : out std_logic
	);
end entity unary_counter_circuit;
 
architecture beh of unary_counter_circuit is

	signal D0 : std_logic;
	signal D0_inv : std_logic;
	signal D1 : std_logic;
	signal D2 : std_logic;
	
	signal res_ff : std_logic;
	signal res_dut : std_logic;
	signal Q_DUT : std_logic;

begin

	res_ff <= res or Q_DUT;
	res_dut <= res or D0_inv;
	D0_inv <= not D0;
	
	flip_flop_1 : D_FF 
		port map (
			clk => clk_ref,
			res => res_ff,
      
			D => '1',
			Q => D0
		);
		
	flip_flop_2 : D_FF 
		port map (
			clk => clk_ref,
			res => res_ff,
      
			D => D0,
			Q => D1
		);
		
	flip_flop_3 : D_FF 
		port map (
			clk => clk_ref,
			res => res_ff,
      
			D => D1,
			Q => D2
		);
		
	flip_flop_4 : D_FF 
		port map (
			clk => clk_ref,
			res => res_ff,
      
			D => D2,
			Q => err
		);
		
	flip_flop_clk_DUT : D_FF 
		port map (
			clk => clk_DUT,
			res => res_dut,
      
			D => '1',
			Q => Q_DUT
		);
	
		
end architecture beh;