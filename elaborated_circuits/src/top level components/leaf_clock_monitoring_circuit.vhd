library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.D_FF_pkg.all;
use work.Schmitt_Trigger_pkg.all;

-- This entity represents the leaf clock monitoring circuit. See figure 7 in the "elaborated_circuits_overview.pdf" file.
-- The corresponding testbench is the "leaf_clock_monitoring_circuit_tb.vhd" file.
entity leaf_clock_monitoring_circuit is
	generic(
		N : integer := 4
	);
	port (
		res : in std_logic;
		clk : in std_logic_vector(N downto 0);
		
		monitor : out  std_logic
		
	);
end entity leaf_clock_monitoring_circuit;
 
architecture beh of leaf_clock_monitoring_circuit is

	signal Q : std_logic_vector(N + 1 downto 0);

begin

	monitor <= Q(0);
	
	Schmitt_Trigger1 : Schmitt_Trigger		-- a dummy smitt trigger
		port map(
			A => Q(N + 1),
			R => Q(0)
		);
	
	--Q(0) <= not Q(N + 1);
	
	flip_flops : for i in N downto 0 generate
		flip_flop : D_FF 
			port map (
				clk => clk(i),
				res => res,
      
				D => Q(i),
				Q => Q(i + 1)
			);
	end generate;
	
end architecture;