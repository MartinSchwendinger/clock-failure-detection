# Clock Failure Detection


This repository stores the VHDL Code, ModelSim and Quartus Project to the Bachelor thesis "Clock Failure Detection". The thesis provides an overview of circuits that detect a clock failure (forever missing edge) with very low latency (< 1 cyc). It also comes up with a bunch of self developed circuits for that purposes. As part a their analysis most of them have been implemented in VHDL simulated via ModelSim. These repository stores the code to do so.

## _elaborated\_circuits_

This folder contains the VHDL-code corresponding to the eponymous chapter. It also contains a ModelSim Project for Windows, so one can retrace the simulations done for the thesis. There is a testbench in the tb subfolder for every circuit in the src sub folder. See the _elaborated\_circuits\_overview.pdf_ for an overview.

## _pure\_delay\_demo_

This folder contains a quartus project synthesizing two pure delay circuits in a test design with two clock muxes. The clock muxes switch to a backup clock, if a pure delay circuit detects a clock failure. The clocks feed a counter, whichs value is printed on the seven segment display periodically, whenever it overflows. By deviations of the counter values one can spot a (or even not spot) a clock failure, depending on the (glitch-)filter that is configured. Also a oscilloscop can be connected to check what is going on inside. See the _pure\_delay\_demo\_overview.pdf_ file.