library ieee;
use ieee.std_logic_1164.all;

LIBRARY altera;
use altera.altera_primitives_components.all;

entity gC2 is 
	port (
		A : in std_logic;
		B : in std_logic;
		Q : out std_logic
	);
end entity;


architecture arch of gC2 is
	component gC2_lut is 
		port (
			A : in std_logic;
			B : in std_logic;
			Q : out std_logic;
			Q_old : in std_logic
		);
	end component;
	
	signal feedback_wire : std_logic;
begin
	C2_inst : gC2_lut
	port map (
		A     => A,
		B     => B,
		Q     => feedback_wire,
		Q_old => feedback_wire
	);
	Q <= feedback_wire;
end architecture;


