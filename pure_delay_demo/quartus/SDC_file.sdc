# Clock constraints
#create_clock -name "CLK_1" -period 20.000ns [get_ports {clk_AG14}]
#create_clock -name "CLK_2" -period 20.000ns [get_ports {clk_AG15}]
create_clock -name "CLK_PIN" -period 20.000ns [get_ports {clk_Y2}]

#set_false_path -from [get_cells {clock_mux_1_inst|pure_delay_circuit_inst|delay|\delay_loop:0:LCELLs}] -to [get_cells {clock_mux_1_inst|pure_delay_circuit_inst|delay|\delay_loop:23:LCELLs}]
#set_false_path -from [get_cells {clock_mux_2_inst|pure_delay_circuit_inst|delay|\delay_loop:0:LCELLs}] -to [get_cells {clock_mux_2_inst|pure_delay_circuit_inst|delay|\delay_loop:23:LCELLs}]

#create_generated_clock -name clk_D -source [get_nets {clock_mux_1_inst|pure_delay_circuit_inst|delay|\delay_loop:0:LCELLs}] -phase 270 [get_nets {clock_mux_1_inst|pure_delay_circuit_inst|delay|\delay_loop:24:LCELLs}]
#create_generated_clock -name clk_D2 -source [get_nets {clock_mux_2_inst|pure_delay_circuit_inst|delay|\delay_loop:0:LCELLs}] -phase 270 [get_nets {clock_mux_2_inst|pure_delay_circuit_inst|delay|\delay_loop:24:LCELLs}]


# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

