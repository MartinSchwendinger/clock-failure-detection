library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.delay_sig;

entity pure_delay_circuit is
	generic(
		LCELL_CLK_D_DELAY : natural := 20;
		LCELL_PULSE_DELAY : natural := 4
	);
	port
	(
		clk_DUT : in std_logic;
      
		errH : out std_logic;
		errL : out std_logic
	);
end entity pure_delay_circuit;
 
architecture beh of pure_delay_circuit is

	signal clk_D : std_logic := '0';
	signal clk_D_delay : std_logic := '0';
	signal pulse_rising : std_logic := '0';
	signal pulse_falling : std_logic := '0';
	signal D1 : std_logic := '0';
	signal D2 : std_logic := '0';
	signal Q1 : std_logic := '0';
	signal Q2 : std_logic := '0';
	signal nQ1 : std_logic := '0';
	signal nQ2 : std_logic := '0';

begin

	clk_D_delay_inst : delay_sig
		generic map(
			LCELL_DELAY => LCELL_CLK_D_DELAY
		)
		port map(
			A => clk_DUT,
			R => clk_D
		);
		
	edge_detector_delay_inst : delay_sig
		generic map(
			LCELL_DELAY => LCELL_PULSE_DELAY
		)
		port map(
			A => clk_D,
			R => clk_D_delay
		);
		
	pulse_rising <= clk_D and (not clk_D_delay);
	pulse_falling <= (not clk_D) and clk_D_delay;
	
	-- D-Latch H
	D1 <= pulse_rising and (not clk_DUT);
	Q1 <= not (nQ1 or (pulse_rising and (not D1)));
	nQ1 <= not (Q1 or (pulse_rising and D1));
	
	-- D-Latch L
	D2 <= pulse_falling and (clk_DUT);
	Q2 <= not (nQ2 or (pulse_falling and (not D2)));
	nQ2 <= not (Q2 or (pulse_falling and D2));
	
	-- err
	errH <= nQ1;
	errL <= nQ2;
		
end architecture beh;