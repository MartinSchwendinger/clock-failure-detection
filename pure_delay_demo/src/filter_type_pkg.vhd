library ieee;
use ieee.std_logic_1164.all;

package filter_type_pkg is

	type FILTER_T is (MULLER_C, SYNC_MUX_FILTER, NOTHING);
	
end filter_type_pkg;
