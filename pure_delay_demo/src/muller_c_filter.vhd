library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cgate_pkg.all;
use work.pure_delay_circuit;
use work.delay_sig;

entity muller_c_filter is
	generic(
		LCELL_FILTER_DELAY : natural := 25
	);
	port(
		res : in std_logic;
		clk_in : in std_logic;
		clk_out : out std_logic
	);
end entity;

architecture muller_c_filter_arch of muller_c_filter is

	signal feedback : std_logic := '0';
	signal clk_out_sig : std_logic := '0';
	
begin
	
	muller_c : C2_r
		port map(
			A => clk_in,
			B => (not feedback),
			Q => clk_out_sig,
			R => res
		);
		
	clk_out <= clk_out_sig;
		
	delay_sig_inst : delay_sig
		generic map(
			LCELL_DELAY => LCELL_FILTER_DELAY
		)
		port map(
			A => clk_out_sig,
			R => feedback
		);
	
end architecture;