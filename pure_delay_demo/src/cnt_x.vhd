library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cnt_x is
	generic(
		CNT_MAX : natural := 10000
	);
	port(
		res : in std_logic;
		clk_Fx : in std_logic;
		oflo_x : out std_logic
	);
end entity;

architecture cnt_x_arch of cnt_x is

	signal cnt : integer range 0 to CNT_MAX := 0;
	
begin

	counter_proc : process(all)
	begin
		if res = '1' then
			cnt <= 0;
			oflo_x <= '0';
		elsif rising_edge(clk_Fx) then
			if cnt < CNT_MAX then
				cnt <= cnt + 1;
			else
				cnt <= 0;
				oflo_x <= not oflo_x;
			end if;
		end if;
	end process;

end architecture;