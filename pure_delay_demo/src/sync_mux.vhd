library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.D_FF;

entity sync_mux is
	generic(
		SYNC_STAGES : natural := 3
	);
	port (
		res : in std_logic;
		sel : in std_logic;
		clk_A : in std_logic;
		clk_B : in std_logic;
		clk_Fx : out std_logic
	);
end entity;

architecture sync_mux_arch of sync_mux is

	signal QA : std_logic_vector(SYNC_STAGES downto 0);
	signal QB : std_logic_vector(SYNC_STAGES downto 0);
	signal Q_A : std_logic;
	signal Q_B : std_logic;
	signal D0_A : std_logic;
	signal D0_B : std_logic;
	
begin
	
	QA(0) <= D0_A;
	Q_A <= QA(SYNC_STAGES);
	sync_A_inst : for i in 0 to SYNC_STAGES - 1 generate
		flip_flop_inst : D_FF
			port map(
				clk => clk_A,
				res => res or (not sel),
				D => QA(i),
				Q => QA(i + 1)
			);
	end generate;
			
	QB(0) <= D0_B;
	Q_B <= QB(SYNC_STAGES);
	sync_B_inst : for i in 0 to SYNC_STAGES - 1 generate
		flip_flop_inst : D_FF
			port map(
				clk => clk_B,
				res => res,
				D => QB(i),
				Q => QB(i + 1)
			);
	end generate;
			
	D0_A <= sel and (not Q_B); --inv
	D0_B <= (not sel) and (not Q_A);
	
	clk_Fx <= (clk_A and Q_A) or (clk_B and Q_B);
	
	
end architecture;