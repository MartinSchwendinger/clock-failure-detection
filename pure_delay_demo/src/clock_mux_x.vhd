library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pure_delay_circuit;
use work.muller_c_filter;
use work.sync_mux;
use work.filter_type_pkg.all;

entity clock_mux_x is
	generic(
		SYNC_STAGES : natural := 3;
		LCELL_CLK_D_DELAY : natural := 20;
		LCELL_PULSE_DELAY : natural := 4;
		LCELL_FILTER_DELAY : natural := 50
	);
	port(
		res : in std_logic;
		filter : in FILTER_T;
		clk_A : in std_logic;
		clk_B : in std_logic;
		clk_Fx : out std_logic;
		errH : out std_logic;
		errL : out std_logic;
		
		debug_muller_C_in : out std_logic;
		debug_muller_C_out : out std_logic
	);
end entity;

architecture clock_mux_x_arch of clock_mux_x is

	signal err : std_logic := '0';
	signal err_cd : std_logic := '0';
	signal muller_c_input : std_logic := '0';
	signal muller_c_output : std_logic := '0';
	signal sync_mux_output : std_logic := '0';

begin

	pure_delay_circuit_inst : pure_delay_circuit
		generic map(
			LCELL_CLK_D_DELAY => LCELL_CLK_D_DELAY,
			LCELL_PULSE_DELAY => LCELL_PULSE_DELAY
		)
		port map(
			clk_DUT => clk_A,
			errH => errH,
			errL => errL
		);
	
	muller_c_input <= clk_B when errL = '1' or errH = '1' else
					      clk_A;
					
	muller_c_filter_inst : muller_c_filter
		generic map(
			LCELL_FILTER_DELAY => LCELL_FILTER_DELAY
		)
		port map(
			res => res,
			clk_in => muller_c_input,
			clk_out => muller_c_output
		);
	debug_muller_C_in <= muller_c_input;
	debug_muller_C_out <= muller_c_output;
	
	sync_mux_inst : sync_mux
		generic map(
			SYNC_STAGES => SYNC_STAGES
		)
		port map(
			res => res, 
			sel => not (errL or errH),
			clk_A => clk_A,
			clk_B => clk_B, 
			clk_Fx => sync_mux_output
		);
	
	clk_Fx <= clk_A when errL = '0' and errH = '0' else 
				 muller_c_output when filter = MULLER_C else
				 sync_mux_output when filter = SYNC_MUX_FILTER else
			    muller_c_input;

end architecture;