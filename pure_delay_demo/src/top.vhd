library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pll_clk_A;
use work.pll_clk_B;
use work.pll_clk_ref;
use work.clock_mux_x;
use work.cnt_x;
use work.disp_x;
use work.sync;
use work.debounce;
use work.filter_type_pkg.all;

entity top is
	port (
		--50 MHz clock input
		clk_Y2    : in std_logic;
		clk_AG14  : in std_logic;
		clk_AG15  : in std_logic;
		clk_SMA : in std_logic;
		
		-- push buttons and switches
		keys     : in std_logic_vector(3 downto 0);
		switches : in std_logic_vector(17 downto 0);
	
		--Seven segment digit
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0);
		hex4 : out std_logic_vector(6 downto 0);
		hex5 : out std_logic_vector(6 downto 0);
		hex6 : out std_logic_vector(6 downto 0);
		hex7 : out std_logic_vector(6 downto 0);

		--the green LEDs
		ledg : out std_logic_vector(8 downto 0);
		
		--the red LEDs
		ledr : out std_logic_vector(17 downto 0);
		
		-- oscilloscope
		osci_1 : out std_logic;
		osci_2 : out std_logic;
		osci_3 : out std_logic;
		osci_4 : out std_logic
		
	);
end entity;

architecture top_arch of top is

constant DEBOUNCE_SWITCHES : natural := 20;
constant SYNC_STAGES_INPUT : natural := 3;
constant SYNC_STAGES_CLK_MUX : natural := 3;

constant REG_SIZE_DISP : natural := 16;
constant CNT_DISP_MAX : natural := 2**REG_SIZE_DISP - 1;
constant CNT_CNT_MAX : natural := 2**26 - 1;

constant LCELL_CLK_D_DELAY_1 : natural := 20;
constant LCELL_CLK_D_DELAY_2 : natural := 20;
constant LCELL_PULSE_DELAY_1 : natural := 4;
constant LCELL_PULSE_DELAY_2 : natural := 4;

constant LCELL_FILTER_DELAY_1 : natural := 10;
constant LCELL_FILTER_DELAY_2 : natural := 10;

--reset and basic clocks
signal res : std_logic := '1';
signal res_disp : std_logic := '1';
signal clk_ref : std_logic := '0';
signal clk_1 : std_logic := '0';
signal clk_2 : std_logic := '0';
signal clk_A : std_logic := '0';
signal clk_B : std_logic := '0';
signal clk_F1 : std_logic := '0';
signal clk_F2 : std_logic := '0';
signal oflo_1 : std_logic := '0';
signal oflo_2 : std_logic := '0';

--clk enable signals
signal clk_1_en : std_logic := '0';
signal clk_2_en : std_logic := '0';
signal force_to_en_1 : std_logic := '0';
signal force_to_val_1 : std_logic := '0';
signal force_to_en_2 : std_logic := '0';
signal force_to_val_2 : std_logic := '0';

-- select filter
signal sel_filter_1 : FILTER_T := MULLER_C;
signal sel_filter_2 : FILTER_T := MULLER_C;

-- err
signal errH_1 : std_logic := '0';
signal errL_1 : std_logic := '0';
signal errH_2 : std_logic := '0';
signal errL_2 : std_logic := '0';

signal debug_muller_C_in : std_logic;
signal debug_muller_C_out : std_logic;

begin

	--------------inputs-------------------

	reset_sync : sync
		generic map(
			SYNC_STAGES => SYNC_STAGES_INPUT,
			RESET_VALUE => '0'
		)
		port map(
			clk => clk_ref,
			res_n => '0',

			data_in  => not keys(0),
			data_out => res
	);
	
	reset_disp_sync : sync
		generic map(
			SYNC_STAGES => SYNC_STAGES_INPUT,
			RESET_VALUE => '0'
		)
		port map(
			clk => clk_ref,
			res_n => '0',

			data_in  => not keys(1),
			data_out => res_disp
	);
	
	debounce_sw0 : debounce
		generic map(
			counter_size => DEBOUNCE_SWITCHES
		)
		port map(
			clk => clk_ref,
			button => switches(0),
			result => clk_1_en
		);
		
	debounce_sw1 : debounce
		generic map(
			counter_size => DEBOUNCE_SWITCHES
		)
		port map(
			clk => clk_ref,
			button => switches(1),
			result => clk_2_en
		);
		
	force_to_en_1 <= switches(2);
	force_to_val_1 <= switches(3);
	force_to_en_2 <= switches(4);
	force_to_val_2 <= switches(5);
	
	-- choose filter
	sel_filter_1 <= MULLER_C when switches(7 downto 6) = "00" else
						 SYNC_MUX_FILTER when switches(7 downto 6) = "01" else
						 NOTHING;
	sel_filter_2 <= MULLER_C when switches(9 downto 8) = "00" else
						 SYNC_MUX_FILTER when switches(9 downto 8) = "01" else
						 NOTHING;
	--osci			 
	osci_1 <= clk_A when switches(10) = '0' else
				 clk_B;
	osci_2 <= errH_1 or errL_1;
	osci_3 <= clk_F1;
	osci_4 <= oflo_1;
	--osci_1 <= debug_muller_C_in;
	--osci_4 <= debug_muller_C_out;
						 
	-------------outputs-------------------
	
	ledg(8) <= '0';
	ledg(7) <= errH_1 or errL_1;
	ledg(6) <= errH_2 or errL_2;
	ledg(5) <= '0';
	ledg(4) <= errH_1;
	ledg(3) <= errL_1;
	ledg(2) <= errH_2;
	ledg(1) <= errL_2;
	ledg(0) <= res;
	
	ledr(0) <= switches(0);
	ledr(1) <= switches(1);
	ledr(2) <= clk_A;
	ledr(3) <= clk_B;
	ledr(15 downto 4) <= (others => '0');
	ledr(16) <= oflo_1 and switches(16);
	ledr(17) <= oflo_2 and switches(17);
						 
	-------------plls----------------------
	
	pll_clk_A_inst : pll_clk_A
		port map(
			--clkswitch => switches(11),
			inclk0 => clk_Y2,    -- 50MHz
			--inclk1 => clk_SMA,	-- 49.99MHz
			c0	=> clk_1				-- 100MHz
		);
		
	pll_clk_B_inst : pll_clk_B
		port map(
			inclk0 => clk_Y2,    -- 50MHz
			c0	=> clk_2	         -- 100MHz
		);
		
	pll_clk_ref_inst : pll_clk_ref
		port map(
			inclk0 => clk_Y2,    -- 50MHz
			c0 => clk_ref        -- 220MHz
		--	c1 => open
		);

	------------clock latches---------------
	
	clk_A <= clk_1 when clk_1_en = '1' else
				force_to_val_1 when force_to_en_1 = '1' else
				clk_A;
	
	clk_B <= clk_2 when clk_2_en = '1' else
				force_to_val_2 when force_to_en_2 = '1' else
				clk_B;
				
	--------------clock muxes------------------
		
	clock_mux_x_inst1 : clock_mux_x
		generic map(
			SYNC_STAGES => SYNC_STAGES_CLK_MUX,
			LCELL_CLK_D_DELAY => LCELL_CLK_D_DELAY_1,
			LCELL_PULSE_DELAY => LCELL_PULSE_DELAY_1,
			LCELL_FILTER_DELAY => LCELL_FILTER_DELAY_1
		)
		port map(
			res => res,
			filter => sel_filter_1,
			clk_A => clk_A,
			clk_B => clk_B,
			clk_Fx => clk_F1,
			errH => errH_1,
			errL => errL_1,
			
			debug_muller_C_in => debug_muller_C_in,
			debug_muller_C_out => debug_muller_C_out
		);
		
	clock_mux_x_inst2 : clock_mux_x
		generic map(
			SYNC_STAGES => SYNC_STAGES_CLK_MUX,
			LCELL_CLK_D_DELAY => LCELL_CLK_D_DELAY_2,
			LCELL_PULSE_DELAY => LCELL_PULSE_DELAY_2,
			LCELL_FILTER_DELAY => LCELL_FILTER_DELAY_2
		)
		port map(
			res => res,
			filter => sel_filter_2,
			clk_A => clk_A,
			clk_B => clk_B,
			clk_Fx => clk_F2,
			errH => errH_2,
			errL => errL_2,
			
			debug_muller_C_in => open,
			debug_muller_C_out => open
		);
		
	-----------------CNTs--------------
		
	cnt1_inst : cnt_x
		generic map(
			CNT_MAX => CNT_CNT_MAX
		)
		port map(
			res => res,
			clk_Fx => clk_F1,
			oflo_x => oflo_1
		);
		
	cnt2_inst : cnt_x
		generic map(
			CNT_MAX => CNT_CNT_MAX
		)
		port map(
			res => res,
			clk_Fx => clk_F2,
			oflo_x => oflo_2
		);
		
	------------disp---------------
		
	dip_1_inst : disp_x
		generic map(
			CNT_MAX => CNT_DISP_MAX,
			REG_SIZE => REG_SIZE_DISP
		)
		port map(
			res => res,
			res_disp => res_disp,
			clk_ref => clk_ref,
			oflo_x => oflo_1,
			hex0 => hex0,
			hex1 => hex1,
			hex2 => hex2,
			hex3 => hex3
		);
		
	dip_2_inst : disp_x
		generic map(
			CNT_MAX => CNT_DISP_MAX,
			REG_SIZE => REG_SIZE_DISP
		)
		port map(
			res => res,
			res_disp => res_disp,
			clk_ref => clk_ref,
			oflo_x => oflo_2,
			hex0 => hex4,
			hex1 => hex5,
			hex2 => hex6,
			hex3 => hex7
		);
		
end architecture;