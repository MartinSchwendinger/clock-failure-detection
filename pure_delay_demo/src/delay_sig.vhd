library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity delay_sig is

	generic(
		DELAY : time := 60 ns;
		LCELL_DELAY : natural := 15
	);
	
	port (
		A : in std_logic;
		R : out std_logic
	);
end entity delay_sig;


architecture delay_quartus of delay_sig is
	
	signal logic_cell_delay : std_logic_vector(LCELL_DELAY downto 0);
	
	COMPONENT LCELL
		PORT (
			a_in : IN STD_LOGIC;
			a_out : OUT STD_LOGIC
		);
	END COMPONENT;
	
begin

	logic_cell_delay(0) <= A;
	R <= logic_cell_delay(LCELL_DELAY);

	delay_loop : for i in LCELL_DELAY - 1 downto 0 generate
		LCELLs : LCELL
			port map(
				a_in => logic_cell_delay(i),
				a_out => logic_cell_delay(i + 1)
			);
	end generate;
		
end architecture;
 