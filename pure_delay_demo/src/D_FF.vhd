library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity D_FF is
	port
	(
		clk : in std_logic;
		res : in std_logic;
      
		D : in std_logic;
		Q : out std_logic
	);
end entity D_FF;
 
architecture D_FF_arch of D_FF is

begin
	process (all) is
    begin
		if res = '1' then
			Q <= '0';
		elsif rising_edge(clk) then  
			Q <= D;
        end if;
	end process;
end architecture;