library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.D_FF;

entity disp_x is
	generic(
		CNT_MAX : natural := 65_535;
		REG_SIZE : natural := 16
	);
	port (
		res : in std_logic;
		res_disp : in std_logic;
		clk_ref : in std_logic;
		oflo_x : in std_logic;
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0)
	);
end entity;

architecture disp_x_arch of disp_x is

	function hex_to_seve_seg (
		signal input : std_logic_vector(3 downto 0))
		return std_logic_vector is
		variable output : std_logic_vector(6 downto 0);
	BEGIN
		CASE input IS
			WHEN "0000" => 
				output := "1000000";
			WHEN "0001" => 
				output := "1111001";
			WHEN "0010" => 
				output := "0100100";
			WHEN "0011" => 
				output := "0110000";
			WHEN "0100" => 
				output := "0011001";
			WHEN "0101" => 
				output := "0010010";
			WHEN "0110" => 
				output := "0000010";
			WHEN "0111" => 
				output := "1111000";
			WHEN "1000" => 
				output := "0000000";
			WHEN "1001" => 
				output := "0010000";
			WHEN "1010" => 
				output := "0001000";
			WHEN "1011" => 
				output := "0000011";
			WHEN "1100" => 
				output := "0100111";
			WHEN "1101" => 
				output := "0100001";
			WHEN "1110" => 
				output := "0000110";
			WHEN others => 
				output := "0001110";
		END CASE;
		return std_logic_vector(output);
	end hex_to_seve_seg;

	signal res_cnt : std_logic := '1';
	signal cnt : integer range 0 to CNT_MAX := 0;
	signal count_val : std_logic_vector(REG_SIZE - 1 downto 0) := (others => '0');
	signal count_val_reg : std_logic_vector(REG_SIZE - 1 downto 0) := (others => '0');
	
	signal Q0 : std_logic := '0';
	signal Q1 : std_logic := '0';
	signal Q2 : std_logic := '0';
	
begin
	
	counter_proc : process(all)
	begin
		if res_cnt = '1' or res = '1' then 
			cnt <= 0;
		elsif rising_edge(clk_ref) then
			if cnt < CNT_MAX then
				cnt <= cnt + 1;
			else
				cnt <= 0;
			end if;
		end if;
	end process;
	
	count_val <= std_logic_vector(to_unsigned(cnt, 16));
	
	sync_FFs : process(all)
	begin
		if res = '1' then
			Q0 <= '0';
			Q1 <= '0';
			Q2 <= '0';
			count_val_reg <= (others => '0');
		elsif rising_edge(clk_ref) then
			Q0 <= oflo_x;
			Q1 <= Q0;
			Q2 <= Q1;
			if res_disp = '1' then
				count_val_reg <= (others => '0');
			end if;
			if Q2 = '0' and Q1 = '1' then
				count_val_reg <= count_val;
			end if;
		end if;
	end process;
	
	res_cnt <= Q2; 
	
	--seven segment
	hex0 <= hex_to_seve_seg(count_val_reg(3 downto 0));
	hex1 <= hex_to_seve_seg(count_val_reg(7 downto 4));
	hex2 <= hex_to_seve_seg(count_val_reg(11 downto 8));
	hex3 <= hex_to_seve_seg(count_val_reg(15 downto 12));
	
end architecture;